from kivy.app import App
from GridLayout.Menus import Menus


class MyApp(App):

    def build(self):
        menus = Menus()
        return menus.fenetre


if __name__ == "__main__":
    MyApp().run()
