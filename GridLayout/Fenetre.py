from kivy.uix.gridlayout import GridLayout


class Fenetre(GridLayout):
    def __init__(self, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)

    def afficher_menu(self, menu):
        self.clear_widgets()
        self.add_widget(menu)