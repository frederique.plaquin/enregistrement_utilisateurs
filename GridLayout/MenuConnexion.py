from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
import os
import json
from Fichiers.saveUser import SaveUser
from Fichiers.userAfficher import UserAfficher
from GridLayout.MenuImages import MenuImages


class MenuConnexion(GridLayout):
    def __init__(self, menus, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.menus = menus
        self.grid1 = GridLayout(cols=1, rows=2)
        self.add_widget(self.grid1)
        self.layout1()

    def layout1(self):
        self.grid1.clear_widgets()
        button_register = Button(text="S' inscrire")
        button_register.bind(on_press=self.clbk_menu_register)
        self.grid1.add_widget(button_register)

        button_label = Label(text="Se connecter")
        self.grid1.add_widget(button_label)

        path = "users.json"
        isExist = os.path.exists(path)
        if isExist:
            # button_login.bind(on_press=se
            fileObject = open("users.json", "r")
            jsonContent = fileObject.read()
            aList = json.loads(jsonContent)
            nb_rows = len(aList['users'])
            nb_rows += 1
            self.grid1.rows = len(aList['users']) + 2
            for i in range(len(aList['users'])):
                user_name = "Id : " + aList["users"][i]["Id"] + ' - ' + aList["users"][i]["Prénom"] + ' ' + \
                            aList["users"][i]["Nom"]
                button_user = Button(text=user_name)
                button_user.bind(on_press=self.clbk_menu_images)
                self.grid1.add_widget(button_user)

            fileObject.close()
        else:
            print("###################################")
            self.grid1.rows = 3
            no_members_label = Label(text="Soyez le premier à vous inscrire !")
            self.grid1.add_widget(no_members_label)

    def callback(self, widget):
        user_nom = self.input_name.text
        user_prenom = self.input_prenom.text
        user_age = self.input_age.text
        SaveUser(user_nom, user_prenom, user_age)
        self.menus.menu_connexion.layout1()

    def clbk_menu_register(self, widget):
        self.menus.afficher_menu(self.menus.menu_register)

    def clbk_menu_images(self, widget):
        UserAfficher(widget)
        self.menus.menu_images.layout1()
        self.menus.afficher_menu(self.menus.menu_images)


