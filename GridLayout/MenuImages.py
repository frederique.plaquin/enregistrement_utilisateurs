from kivy.uix.gridlayout import GridLayout
import os
import json
from kivy.uix.label import Label
from Texte.Bouton import Bouton
from kivy.uix.carousel import Carousel
from kivy.uix.image import AsyncImage
from kivy.clock import Clock


class MenuImages(GridLayout):
    def __init__(self, menus, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.menus = menus
        self.grid1 = GridLayout(cols=1, rows=3)
        self.add_widget(self.grid1)
        Clock.schedule_once(self.delay, 5)

    def delay(self, dt):
        self.layout1()

    def layout1(self):
        self.grid1.clear_widgets()
        path = "user_afficher.json"
        isExist = os.path.exists(path)
        if isExist:
            fileObject = open("user_afficher.json", "r")
            jsonContent = fileObject.read()
            aList = json.loads(jsonContent)
            user_id = aList['Id']
            fileObject.close()

            fileObject = open("users.json", "r")
            jsonContent = fileObject.read()
            aList = json.loads(jsonContent)
            for i in range(len(aList['users'])):
                if user_id == aList["users"][i]["Id"]:
                    user_label = Label(text=aList["users"][i]["Prénom"] + ' ' + aList["users"][i]["Nom"] + ' - ' + aList["users"][i]["Âge"] + " ans")
                    self.grid1.add_widget(user_label)
            fileObject.close()
            carousel_widget = self.carousel()
            self.grid1.add_widget(carousel_widget)
            b1 = Bouton(text="Quitter")
            b1.bind(on_press=self.clbk_menu_connexion)
            self.grid1.add_widget(b1)

    def carousel(self):
        carousel_widget = Carousel(direction='right', anim_type='out_quad')
        images = ["./images/bob1.webp", "./images/carlo.jpg", "./images/patrick.jpg"]
        for item in images:
            src = item
            image = AsyncImage(source=src, allow_stretch=True)
            carousel_widget.add_widget(image)

        return carousel_widget

    def clbk_menu_connexion(self, widget):
        self.menus.afficher_menu(self.menus.menu_connexion)
