from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from Texte.Bouton import Bouton
from Fichiers.saveUser import SaveUser


class MenuRegister(GridLayout):
    def __init__(self, menus, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.menus = menus
        self.grid1 = GridLayout(cols=2, rows=4)
        self.add_widget(self.grid1)
        self.label_name = Label(text="Nom")
        self.input_name = TextInput(text="")
        self.label_prenom = Label(text="Prénom")
        self.input_prenom = TextInput(text="")
        self.label_age = Label(text="Age")
        self.input_age = TextInput(text="")
        self.register_button = Bouton(text="S' inscrire")
        self.cancel_button = Button(text="Annuler")
        self.register_button.bind(on_press=self.callback)
        self.cancel_button.bind(on_press=self.clbk_menu_connexion)
        self.grid1.add_widget(self.label_name)
        self.grid1.add_widget(self.input_name)
        self.grid1.add_widget(self.label_prenom)
        self.grid1.add_widget(self.input_prenom)
        self.grid1.add_widget(self.label_age)
        self.grid1.add_widget(self.input_age)
        self.grid1.add_widget(self.register_button)
        self.grid1.add_widget(self.cancel_button)

    def callback(self, widget):
        user_nom = self.input_name.text
        user_prenom = self.input_prenom.text
        user_age = self.input_age.text
        SaveUser(user_nom, user_prenom, user_age)
        self.menus.menu_connexion.layout1()

    def clbk_menu_connexion(self, widget):
        self.grid1.clear_widgets()
        self.menus.afficher_menu(self.menus.menu_connexion)
