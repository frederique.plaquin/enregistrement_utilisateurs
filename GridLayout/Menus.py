from GridLayout.Fenetre import Fenetre
from GridLayout.MenuConnexion import MenuConnexion
from GridLayout.MenuRegister import MenuRegister
from GridLayout.MenuImages import MenuImages

class Menus:
    def __init__(self, **kwargs):
        self.fenetre = Fenetre()
        self.menu_connexion = MenuConnexion(self)
        self.menu_register = MenuRegister(self)
        self.menu_images = MenuImages(self)

        self.fenetre.afficher_menu(self.menu_connexion)

    def afficher_menu(self, menu):
        self.fenetre.afficher_menu(menu)