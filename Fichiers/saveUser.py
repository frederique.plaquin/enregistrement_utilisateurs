import xml.etree.ElementTree as xml
import json


class SaveUser:
    def __init__(self, user_nom, user_prenom, user_age):
        self.xml_file()
        self.json_file()
        self.user_nom = user_nom
        self.user_prenom = user_prenom
        self.user_age = user_age
        self.register_user_xml()
        self.register_user_json()

    def xml_file(self):
        try:
            with open('users.xml'):
                pass
        except IOError:
            fichier = CreateXML()

    def json_file(self):
        try:
            with open('users.json'):
                pass
        except IOError:
            fichier = CreateJSON()

    def register_user_xml(self):
        # determine user id
        filename = 'users.xml'
        tree = xml.ElementTree(file=filename)

        tree.parse("users.xml")

        root = tree.getroot()

        users = root.findall('user')
        nb_users = len(users)


        user_id = "00000" + str(nb_users + 1)

        # register user XML
        filename = 'users.xml'
        tree = xml.ElementTree(file=filename)
        root = tree.getroot()
        user1 = xml.Element('user')
        root.append(user1)
        user1_user_id = xml.SubElement(user1, 'id')
        user1_user_id.text = user_id
        name = self.user_nom
        user1_name = xml.SubElement(user1, 'nom')
        user1_name.text = name
        prenom = self.user_prenom
        user1_prenom = xml.SubElement(user1, 'prenom')
        user1_prenom.text = prenom
        prenom = self.user_age
        user1_age = xml.SubElement(user1, 'age')
        user1_age.text = prenom

        tree = xml.ElementTree(root)
        with open(filename, "wb") as files:
            tree.write(files)

    def register_user_json(self):
        # determine user id
        fileObject = open("users.json", "r")
        jsonContent = fileObject.read()
        aList = json.loads(jsonContent)
        nb_users = len(aList['users'])
        user_id = "00000" + str(nb_users + 1)
        fileObject.close()

        with open("users.json") as file:
            # First we load existing data into a dict.
            data = json.load(file)
            temp = data["users"]
            # Join new_data with file_data inside emp_details
            new_data = {"Id" : user_id, "Nom": self.user_nom, "Prénom": self.user_prenom, "Âge": self.user_age}
            temp.append(new_data)

            with open("users.json", "w") as outfile:
                json.dump(data, outfile, indent=4)
