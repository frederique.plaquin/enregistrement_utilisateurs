import xml.etree.ElementTree as xml


class CreateXML:
    def __init__(self):
        # create XML file
        filename = "users.xml"
        self.root = xml.Element('users')
        self.tree = xml.ElementTree(self.root)
        with open(filename, "wb") as files:
            self.tree.write(files)