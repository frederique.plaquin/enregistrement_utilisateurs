import json


class UserAfficher:
    def __init__(self, widget):
        string = widget.text
        string = string.split()
        # user_id = widget.id
        # create JSON file
        # Data to be written
        dictionary = {
            "Id": string[2]
        }
        # Serializing json
        json_object = json.dumps(dictionary, indent=4)
        # Writing to sample.json
        with open("user_afficher.json", "w") as outfile:
            outfile.write(json_object)