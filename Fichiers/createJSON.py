import json


class CreateJSON:
    def __init__(self):
        # create JSON file
        # Data to be written
        dictionary = {
            'users': [

            ]
        }
        # Serializing json
        json_object = json.dumps(dictionary, indent=4)
        # Writing to sample.json
        with open("users.json", "w") as outfile:
            outfile.write(json_object)
